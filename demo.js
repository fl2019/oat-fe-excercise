import TestTakerListController from './src/testTakerList/TestTakerListController';
import TestTakerJsonModel from './src/testTakerList/TestTakerJsonModel';
import TestTakerCsvModel from './src/testTakerList/TestTakerCsvModel';

initializeDataSourceSelector();
initializeDemo();

var testTakerList;

function getTestTakerModelConstructor() {
	if (location.hash === '#csv') {
		return TestTakerCsvModel;
	} else {
		return TestTakerJsonModel;
	}
}

function highlightActiveDtaSourceSelector(hash = location.hash || '#json') {
	document.querySelectorAll('#data-source-selector a').forEach(selector => {
		if (selector.getAttribute('href') === hash) {
			selector.classList.add('active');
		} else {
			selector.classList.remove('active');
		}
	});
}

function initializeDataSourceSelector() {
	highlightActiveDtaSourceSelector();
	window.addEventListener('hashchange', reinitializeDemo);
}

function initializeDemo() {
	if (testTakerList) {
		testTakerList.destroy();
	}

	initializeDataSourceSelector();

	testTakerList = new TestTakerListController({
		Model: getTestTakerModelConstructor(),
		viewContainer: document.getElementById('list-container'),
		pageSize: 10
	});
}

function reinitializeDemo() {
	highlightActiveDtaSourceSelector();
	initializeDemo();
}
