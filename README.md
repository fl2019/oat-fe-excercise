# OAT Frontend Exercise

## Documentation & Workflow

To show my workflow and thought process, please refer to `Exercise.md`. Due to time constraints, this file is written in very concise English.

## Demo

To see the component in action, just open `demo.html` in a browser.
