# Exercise: My Thought Process & Workflow

## Assumptions

Since I can't discuss requirements in this exercise with stakeholders, I can only make assumptions about them. So, to clarify, I assume the following:

- There is no backend work to be done
- There is a mock response (testers.csv, testers.json)
- The backend implements the API as documented here: https://hr.oat.taocloud.org/api/
- I can focus entirely on JS code, package management (e.g. npm, yarn) is not relevant to this exercise
- There is no design requirement
- **No EcmaScript version was specified** (see below)
- No TypeScript for implementation (I will use TypeScript in this markdown file to document the software design)


### No EcmaScript version specified
Since the instruction email does not specifiy which ES version (ES3, ES5, ES6, etc) can be used, I will work using EcmaScript version 6.

I am aware that OAT has at least one legacy codebase, which might contain EcmaScript 3 code (especially prototypes instead of classes). For this exercise though I aim to show that I have sufficient skill in software design, rather than focusing on low level language details (prototypes).

### Problems
#### Invalid User Image URLs
The user image URLs are invalid (all are 404 not found). Would like to build a image loader with fallback, but due to time constraints I stick to using "gender" icons for now.



## Frameworks & 3rd Party IP

### JS Framekworks

There are some popular options to implement a component like this:

- React (Storybook for demo)
- Svelte
- Vue.js
- Angular
- MVC, Vanilla

I will implement an MVC component with Vanilla JS because I believe it's important to show the ability to implement code without frameworks.

### 3rd Party IP

- csv-parse: https://csv.js.org/parse
- Icons:
	- Woman Icon: https://www.flaticon.com/free-icon/woman_417776
	- Man Icon: https://www.flaticon.com/free-icon/boss_265674


## Concept

A component is needed that displays a list of strings (user names).

> Nice to have: The component should be reused to display other kinds of data.

In real life, this means we can expect the requirement to display "other kinds of data" in the near future. This is not a problem because a list can be easily customized with a kind of injectable *renderer*.

> enables you to load the data from the service 

To avoid mixing code for data loading and presentation, we need to de-couple carefully.
We will need:

- Data Loading
	- support for multiple data formats (JSON, CSV, ...)
	- ability to load from differnt endpoints (maybe a future requirement)
- Presentation
	- list of data
	- injectable renderer: the requirements for data representation might change


## Performance Considerations

### Paging
The list of data is not limited to a certain size. This means it is not safe to load and/or render the complete list at once. For this reason, the backend `offset` and `limit` parameters in the `GET /users` endpoint.

I assume the list of items in the backend is stable, meaning that any given offset-limit combination will return the same result indepentetly of when a request is made.

Depending on the size of the list, windowing (usually used for "endless scrolling" á la Instagram) could be implemented. Due to time constraints I will only implement simple paging with a "load more" button though.


## Potential Improvements

- Unit Tests for base classes (e.g. `EventEmitter`)
- Windowing support for list component
