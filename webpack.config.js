const path = require('path');
const webpack = require('webpack');

module.exports = {
	entry: './demo.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'demo.js'
	},
	module: {
		rules: [
			{
				test: /\.svg$/,
				loader: 'svg-inline-loader'
			}
		]
	}
};