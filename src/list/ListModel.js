import { AbstractNotImplementedException } from '../util/exception';
import { unsignedFiniteInt } from '../util/typeAssert';

const itemsSymbol = Symbol('items');

/**
 * @abstract
 */
export default class ListModel {
	constructor() {
		/**
		 * @property {array} items All loaded items in the same order as in the data source.
		 * @private
		 */
		this[itemsSymbol] = [];
	}

	destroy() {
		this[itemsSymbol] = undefined;
	}

	/**
	 * Returns the number of items that were loaded already.
	 * @returns {unsigned int} The number of items.
	 */
	getLoadedItemCount() {
		return this[itemsSymbol].length;
	}

	/**
	 * Returns all items the model has already loaded.
	 * @returns {Array} The loaded items.
	 */
	getLoadedItems() {
		// return a clone of the items array to protect internal state
		return [...this[itemsSymbol]];
	}

	/**
	 * Returns items in a given range.
	 * If a requested range has not been loaded yet, the missing items will be loaded
	 * from the data source.
	 * @param {int} offset The offset to start loading items from.
	 * @param {int} limit The number of items to load.
	 * @returns {Array} Array of items in the range.
	 */
	async getItems(offset = 0, limit = 20) {
		unsignedFiniteInt(offset);
		unsignedFiniteInt(limit);

		const items = this[itemsSymbol];
		const loadedItemCount = this.getLoadedItemCount();

		// if the range has been loaded already, return it
		if (offset + limit < loadedItemCount) {
			return items.slice(offset, offset + limit);
		}

		// if the requested range has not been loaded, or has been loaded partially,
		// calculate the missing part of the range and load it 
		let actualLimit = limit;

		if (offset > loadedItemCount) {
			actualLimit += offset - loadedItemCount;
		} else if (offset < loadedItemCount) {
			actualLimit -= loadedItemCount - offset;
		}

		// load the missing part of the range and append the items to this model
		const newItems = await this.loadItems(loadedItemCount, actualLimit);
		items.push(...newItems);

		// range is now loaded in full, so slice it out of all loaded items and return
		return items.slice(offset, offset + limit);
	}

	/**
	 * Load a list of items in a given range from the data source.
	 * @abstract
	 * @protected
	 * @param {int} offset The offset to start loading items from.
	 * @param {int} limit The number of items to load.
	 * @returns {Array} Array of items in the range.
	 */
	async loadItems(offset, limit) {
		throw new AbstractNotImplementedException();
	}
}