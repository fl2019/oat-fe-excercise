import EventEmitter from '../util/EventEmitter';

export default class ListView extends EventEmitter {
	constructor({
		viewContainer,
		renderListItem,
		model
	} = options) {
		super();

		this.viewContainer = viewContainer;
		this.renderListItem = renderListItem;
		this.model = model;

		this.prepareDom();
	}

	destroy() {
		super.destroy();

		this.viewContainer.removeChild(this.listElement);
		this.viewContainer.removeChild(this.loadMoreButton);

		this.loadMoreButton.removeEventListener(
			'click',
			this.loadMoreButtonClickHandler
		)
	}

	prepareDom() {
		this.listElement = document.createElement('ol');
		this.listElement.classList.add('list');
		this.viewContainer.appendChild(this.listElement);

		this.loadMoreButton = document.createElement('div');
		this.loadMoreButton.classList.add('list-load-more-button');
		this.loadMoreButton.textContent = 'Load More';
		this.viewContainer.appendChild(this.loadMoreButton);

		this.loadMoreButtonClickHandler = () => this.emit('click:load-more');
		this.loadMoreButton.addEventListener(
			'click',
			this.loadMoreButtonClickHandler
		);
	}

	setLoadingState(loading) {
		if (loading) {
			this.listElement.classList.add('list-loading');
		} else {
			this.listElement.classList.remove('list-loading');
		}
	}

	renderNewItems(items) {
		items.forEach(item => this.renderSingleItem(item));
	}

	renderSingleItem(itemData) {
		const listRowElement = document.createElement('li');
		listRowElement.classList.add('list-item');
		this.listElement.appendChild(listRowElement);

		this.renderListItem(itemData, listRowElement);
	}
}