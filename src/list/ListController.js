import ListView from './ListView';

export default class ListController {
	constructor({
		Model,
		renderListItem,
		viewContainer,
		pageSize = 20
	} = options) {
		this.model = new Model();
		this.view = new ListView({
			viewContainer,
			renderListItem,
			model: this.model
		});
		this.pageSize = pageSize;
		this.renderedItemsCount = 0;

		/** @private */
		this.onLoadModeButtonClicked = () => this.renderNextRange();
		this.view.on('click:load-more', this.onLoadModeButtonClicked);

		// render the first page
		this.renderNextRange();
	}

	destroy() {
		this.view.off('click:load-more', this.onLoadModeButtonClicked);
		this.view.destroy();
		this.model.destroy();
	}

	/**
	 * Renders the next possible range, depending on `pageSize`.
	 * @public
	 * @returns {void}
	 */
	renderNextRange() {
		const renderPromise = this.renderRange(this.renderedItemsCount, this.pageSize);
		this.renderedItemsCount += this.pageSize;
		return renderPromise;
	}

	/** @private */
	async renderRange(offset, limit) {
		this.view.setLoadingState(true);
		const items = await this.model.getItems(offset, limit);
		this.view.renderNewItems(items);
		this.view.setLoadingState(false);
	}
}