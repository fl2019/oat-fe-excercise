export function assertion(...tests) {
	if (tests.length === 1) {
		return tests[0];
	}

	return value => {
		for (let i = 0; i < tests.length; i++) {
			const test = tests[i];

			if (!test(value)) {
				// throw an error that contains the condition function's body
				// as a string to help debugging
				throw new Error(`Assertion failed: ${test}`);
			}
		}
	};
}

export const typeofNumber = assertion(value => typeof value === 'number');
export const notNaN = assertion(value => !isNaN(value));
export const unsigned = assertion(value => value >= 0);
export const int = assertion(value => Math.round(value) === value);
export const unsignedFiniteInt = assertion(typeofNumber, notNaN, unsigned, int);
