export class AbstractNotImplementedException extends Error {
	constructor() {
		super('Abstract memeber not implemented.');
	}
}
