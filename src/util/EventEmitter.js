const handlerListSymbol = Symbol('handlerList');

export default class EventEmitter {
	constructor() {
		/**
		 * @property {object} handlerList
		 * This contains all bound event handlers. At runtime, this object
		 * has a structure like this:
		 *
		 *     {
		 *        'click': [
		 *             <Function>,
		 *             <Function>,
		 *             <Function>
		 *         ],
		 *         'mouseout': [
		 *             <Function>
		 *         ]
		 *     }
		 */
		this[handlerListSymbol] = {};
	}

	destroy() {
		this[handlerListSymbol] = undefined;
	}

	on(eventName, handler) {
		const handlerList = this[handlerListSymbol];

		handlerList[eventName] = handlerList[eventName] || [];

		// don't bind the same handler twice
		if (handlerList[eventName].indexOf(handler) === -1) {
			handlerList[eventName].push(handler);
		}
	}

	once(eventName, handler) {
		this.on(eventName, (...args) => {
			this.off(eventName, handler);
			handler(...args);
		});
	}

	off(eventName, handler) {
		const handlerList = this[handlerListSymbol];

		if (!handlerList[eventName]) {
			return;
		}

		const index = handlerList[eventName].indexOf(handler);

		if (index === -1) {
			return;
		}

		handlerList[eventName].splice(index, 1);

		// clean up empty event handler lists
		if (handlerList[eventName].length === 0) {
			delete handlerList[eventName];
		}
	}

	emit(eventName, parameters = []) {
		const handlerList = this[handlerListSymbol];

		if (!handlerList[eventName] || handlerList[eventName].length === 0) {
			return;
		}

		handlerList[eventName].forEach(handler => {
			handler(...parameters);
		});
	}
}
