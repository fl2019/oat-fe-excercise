import USER_JSON from './mockData/userJson';
import USER_CSV from './mockData/userCsv';

const ENDPOINTS = {
	'/users': ({
		username,
		offset = 0,
		limit = 20
	}) => {
		return JSON.parse(USER_JSON).slice(offset, offset + limit);
	},

	// This endpoint is not part of the documented swagger API,
	// but it's added here to show the flexibility of the FE code. 
	'/users/csv': () => {
		return USER_CSV;
	}
};

export function mockFetch(endpointUrl, parameters) {
	return new Promise(resolve => {
		const responseData = ENDPOINTS[endpointUrl](parameters);
		const timeout = Math.floor(Math.random() * 600) + 200;

		console.log(`Mock API request: ${endpointUrl}\nResponding after ${timeout}ms`);

		setTimeout(() => resolve(responseData), 300);
	});
}