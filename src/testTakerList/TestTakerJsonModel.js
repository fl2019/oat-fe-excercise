import ListModel from '../list/ListModel';
import { mockFetch } from '../api/api';

export default class TestTakerJsonModel extends ListModel {
	/**
	 * Load a list of items in a given range from the data source.
	 * @abstract
	 * @protected
	 * @param {int} offset The offset to start loading items from.
	 * @param {int} limit The number of items to load.
	 * @returns {Array} Array of items in the range.
	 */
	async loadItems(offset, limit) {
		return mockFetch('/users', { offset, limit });
	}
}