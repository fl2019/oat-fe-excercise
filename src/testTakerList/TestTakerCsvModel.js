import ListModel from '../list/ListModel';
import { mockFetch } from '../api/api';

// External Dependency:
// Used because writing a CSV parser is not in the scope of this exercise.
import parse from 'csv-parse/lib/sync';

export default class TestTakerCsvModel extends ListModel {
	/**
	 * Load a list of items in a given range from the data source.
	 * @abstract
	 * @protected
	 * @param {int} offset The offset to start loading items from.
	 * @param {int} limit The number of items to load.
	 * @returns {Array} Array of items in the range.
	 */
	async loadItems(offset, limit) {
		const data = await this.getParsedCsv();
		return data.slice(offset, offset + limit);
	}

	/**
	 * If no CSV has been loaded and parsed, this loads and parses the CSV and
	 * caches it. Subsequent calls to this method will return the cached CSV.
	 * @private
	 * @returns {object} The parsed CSV.
	 */
	async getParsedCsv() {
		if (this.parsedCsv) {
			return this.parsedCsv;
		}

		// this mock endpoint is not part of the documented API, but
		// it's used here to show how the models can be used to abstract
		// the incoming data (CSV data is a raw string here)
		const rawCsv = await mockFetch('/users/csv');

		// the mock endpoint only returns a string, so we have
		// apply offset and limit ourselves
		this.parsedCsv = parse(rawCsv, {
			columns: true,
			skip_empty_lines: true
		});

		return this.parsedCsv;
	}
}