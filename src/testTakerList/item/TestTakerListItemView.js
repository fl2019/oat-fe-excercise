import womanIcon from '../../icons/woman.svg';
import manIcon from '../../icons/man.svg';

export class TestTakerListItemView {
	constructor({
		model,
		viewContainer
	} = options) {
		this.model = model;
		this.viewContainer = viewContainer;
	}

	destroy() {
		this.viewContainer.innerHTML = '';
	}

	render() {
		const {
			gender,
			picture,
			title,
			firstname,
			lastname,
			login
		} = this.model;

		const genderIcon = gender === 'female' ? womanIcon : manIcon;

		this.viewContainer.innerHTML = `
			<div class="test-taker-list-item">
				<div class="gender-icon">${genderIcon}</div>
				<div class="names">
					<span class="firstname">${firstname}</span>
					<span class="lastname">${lastname}</span>
					<span class="title">(${title})</span>
					<span class="username">@${login}</span>
				</div>
			</div>
		`;
	}
}