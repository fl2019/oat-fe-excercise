import { TestTakerListItemView } from "./TestTakerListItemView";

export class TestTakerListItemController {
	constructor({
		testTakerData,
		viewContainer
	} = options) {
		this.model = testTakerData;
		this.view = new TestTakerListItemView({
			model: this.model,
			viewContainer
		});

		// render the view right away
		this.view.render();
	}

	destroy() {
		this.view.destroy();
	}
}