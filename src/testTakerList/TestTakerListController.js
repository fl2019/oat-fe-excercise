import ListController from '../list/ListController';
import { TestTakerListItemController } from './item/TestTakerListItemController';

export default class TestTakerListController extends ListController {
	constructor({
		Model,
		viewContainer,
		pageSize = null
	} = options) {
		super({
			Model,
			viewContainer,
			pageSize,
			renderListItem: (item, domElement) => this.renderListItem(item, domElement)
		});

		this.listItems = [];
	}

	renderListItem(
		testTakerData,
		viewContainer
	) {
		const listItem = new TestTakerListItemController({
			testTakerData,
			viewContainer
		});
		this.listItems.push(listItem);
	}
}